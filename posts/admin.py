from django.contrib import admin


from posts.models import Post, Comment, Keyword  # NEW: import Keyword


class PostAdmin(admin.ModelAdmin):
    pass


class CommentAdmin(admin.ModelAdmin):
    pass


class KeywordAdmin(admin.ModelAdmin):  # NEW: the whole class
    pass


admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Keyword, KeywordAdmin)  # NEW
